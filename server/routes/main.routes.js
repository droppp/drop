import { Router } from 'express';
import * as MainController from '../controllers/main.controller';
import { verifyToken } from '../server';
const router = new Router();

router.route('/').get(MainController.welcomeMessage);

router.route('/authenticate').post(MainController.authenticateUser);

export default router;
