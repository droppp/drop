import User from './models/user';

export default function () {
  User.count().exec((err, count) => {
    if (count > 0) {
      return;
    }

    const user1 = new User({
      username: 'mange',
      password: 'hejmange',
      fname: 'Thomas',
      lname: 'Kjellberg',
      slug: 'thomas-kjellberg',
      address: 'Kommunalvägen 24, 14161 Huddinge',
      coordinates: { lat: 59.235889, lng: 17.979654 },
      active: true,
      animate: false,
    });

    const user2 = new User({
      username: 'mandish',
      password: 'hejmandish',
      fname: 'Amanda',
      lname: 'Bouveng',
      slug: 'amanda-bouveng',
      address: 'Bergengatan 12, 16435 Kista',
      coordinates: { lat: 59.409137, lng: 17.927462 },
      active: true,
      animate: false,
    });

    const user3 = new User({
      username: 'bananish',
      password: 'hejbananish',
      fname: 'Hannah',
      lname: 'Kransell',
      slug: 'hannah-kransell',
      address: 'Borgargatan 8, 11734 Stockholm',
      coordinates: { lat: 59.317433, lng: 18.034346 },
      active: true,
      animate: false,
    });

    const user4 = new User({
      username: 'ders',
      password: 'hejders',
      fname: 'Anders',
      lname: 'Berg',
      slug: 'anders-berg',
      address: 'Rålambsvägen 67, 11256 Stockholm',
      coordinates: { lat: 59.331171, lng: 17.999391 },
      active: false,
      animate: false,
    });

    const user5 = new User({
      username: 'holmskan',
      password: 'hejholmskan',
      fname: 'Sara',
      lname: 'Holm',
      slug: 'sara-holm',
      address: 'Pingstvägen 34, 12636 Stockholm',
      coordinates: { lat: 59.301817, lng: 17.997386 },
      active: false,
      animate: false,
    });

    User.create([user1, user2, user3, user4, user5], (error) => {
      if (!error) {
        console.log('ready to go....');
      } else {
        console.log('no data injected: ' + error);
      }
    });
  });
}

