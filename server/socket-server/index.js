import io from 'socket.io';
import { webServer } from '../server';
import User from '../models/user';

const setAnimateToFalse = (username, connections, socket) => setTimeout(() => {
  User.findOne({ username }).exec((err, user) => {
    if (err) {
      console.log(err);
    }
    console.log(user);
    user.animate = false;

    connections.forEach(connectedSocket => {
      if (connectedSocket !== socket) {
        connectedSocket.emit('action', { type:'toggleAnimate', user });
      }
    });

    user.save();
  });
  
}, 10000);

const socketServer = (server = webServer) => {
  const socketServer = io(server);
  const connections = [];

  socketServer.on('connection', socket => {
    connections.push(socket);
    console.log(socket.id);
    socket.on('action', action => {
      if (action.active) {
        User.findOne({ username: action.username }).exec((err, user) => {
          if (err) {
            console.log(err);
          }
          console.log(user);
          user.animate = false;

          user.save();
        });
      } else if (!action.active) {
        User.findOne({ username: action.username }).exec((err, user) => {
          if (err) {
            console.log(err);
          }
          console.log(user);
          user.animate = true;
          setAnimateToFalse(action.username, connections, socket);
          user.save();
        });
      }

      switch (action.type) {
        case 'server/add':
          console.log(action);
          User.findOne({ username: action.username }, { username: 1, fname: 1, lname: 1, address: 1, coordinates: 1, active: 1, animate: 1 }).exec((err, user) => {
            if (err) {
              console.log(err);
            }

            user.animate = true;
            
            connections.forEach(connectedSocket => {
              if (connectedSocket !== socket) {
                connectedSocket.emit('action', action.active ? { type:'removeUser', user } : { type:'newUser', user });
              }
            });
          });
          break;
      }

      
    });

    socket.on('disconnect', () => {
      const index = connections.indexOf(socket);
      connections.splice(index, 1);
    });
    
  });
}

export default socketServer;
