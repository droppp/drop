import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const userSchema = new Schema({
  username: { type: 'String', required: true },
  password: { type: 'String', required: true },
  fname: { type: 'String', required: true },
  lname: { type: 'String', required: true },
  address: { type: 'String', required: true },
  coordinates: { type: 'Object', required: true },
  slug: { type: 'String', required: true },
  active: { type: 'Boolean', required: true },
  animate: { type: 'Boolean', required: true },
  dateAdded: { type: 'Date', default: Date.now, required: true },
});

export default mongoose.model('User', userSchema);
