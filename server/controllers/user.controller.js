import User from '../models/user';
import cuid from 'cuid';
import slug from 'limax';
import sanitizeHtml from 'sanitize-html';

/**
 * Get all posts
 * @param req
 * @param res
 * @returns void
 */
export function getUsers(req, res) {
  User.find({ 'active': 'true' }, { username: 1, fname: 1, lname: 1, address: 1, coordinates: 1, active: 1, animate: 1 }).exec((err, users) => {
    if (err) {
      res.status(500).send(err);
    }
    res.json({ users });
  });
}

/**
 * Save a post
 * @param req
 * @param res
 * @returns void
 */
export function addUser(req, res) {
  if (!req.body.user.fname || !req.body.user.lname || !req.body.user.address) {
    res.status(403).end();
  }

  const newUser = new User(req.body.user);

  // Let's sanitize inputs
  newUser.fname = sanitizeHtml(newUser.fname);
  newUser.lname = sanitizeHtml(newUser.lname);
  newUser.address = sanitizeHtml(newUser.address);

  newUser.cuid = cuid();
  newUser.save((err, saved) => {
    if (err) {
      res.status(500).send(err);
    }
    res.json({ user: saved });
  });
}

/**
 * Get a single post
 * @param req
 * @param res
 * @returns void
 */
export function getUser(req, res) {
  User.findOne({ cuid: req.params.cuid }).exec((err, user) => {
    if (err) {
      res.status(500).send(err);
    }
    res.json({ user });
  });
}

/**
 * Delete a post
 * @param req
 * @param res
 * @returns void
 */
export function deleteUser(req, res) {
  User.findOne({ cuid: req.params.cuid }).exec((err, user) => {
    if (err) {
      res.status(500).send(err);
    }

    user.remove(() => {
      res.status(200).end();
    });
  });
}

export function toggleActiveState(req, res) {
  User.findOne({ username: req.body.username }).exec((err, user) => {
    if (err) {
      res.status(500).send(err);
    }

    user.active = !req.body.active;

    user.save((error, saved) => {
      if (error) {
        res.status(500).send(error);
      }
      res.json({ saved, state: !req.body.active });
    });
  });
}
