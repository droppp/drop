import User from '../models/user';
import cuid from 'cuid';
import slug from 'limax';
import sanitizeHtml from 'sanitize-html';
import { getTokenSign } from '../server';

/**
 * Get all posts
 * @param req
 * @param res
 * @returns void
 */
export function welcomeMessage(req, res) {
  res.json({ message: 'Welcome to the coolest API on earth!' });
}

/**
 * Save a post
 * @param req
 * @param res
 * @returns void
 */
export function authenticateUser(req, res) {
  // find the user
  User.findOne({
    username: req.body.username,
  }, function (err, user) {
    if (err) throw err;

    if (!user) {
      res.json({ success: false, message: 'Authentication failed. User not found.' });
    } else if (user) {
      // check if password matches
      if (user.password != req.body.password) {
        res.json({ success: false, message: 'Authentication failed. Wrong password.' });
      } else {
        // if user is found and password is right
        // create a token
        var token = getTokenSign(user);
        // return the information including token as JSON
        res.json({
          success: true,
          message: 'Enjoy your token!',
          token,
          user: {
            username: user.username,
            fname: user.fname,
            lname: user.lname,
            address: user.address,
            coordinates: user.coordinates,
            active: user.active,
          },
        });
      }
    }
  });
}
