import { LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILURE, LOGOUT_SUCCESS, MENU_OPEN, MENU_CLOSE, FETCH_USERS, UPDATE_ROUTE, TOGGLE_ANIMATE } from './AppActions';

// The auth reducer. The starting state sets authentication
// based on a token being in local storage. In a real app,
// we would also want a util to check if the token is expired.
const AppReducer = (state = {
  isFetching: false,
  isAuthenticated: false,
  userInfo: {
    user: {
      coordinates: {
        lat: null,
        lng: null,
      },
    },
  },
  errorMessage: '',
  menuOpen: false,
  users: [],

}, action) => {
  switch (action.type) {
    case LOGIN_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
        isAuthenticated: false,
      });
    case LOGIN_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        isAuthenticated: true,
        errorMessage: '',
        userInfo: action.user,
      });
    case LOGIN_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        isAuthenticated: false,
        errorMessage: action.message,
      });
    case LOGOUT_SUCCESS:
      return Object.assign({}, state, {
        isFetching: true,
        isAuthenticated: false,
      });
    case MENU_OPEN:
      return Object.assign({}, state, {
        menuOpen: true,
      });
    case MENU_CLOSE:
      return Object.assign({}, state, {
        menuOpen: false,
      });
    case FETCH_USERS:
      return Object.assign({}, state, {
        users: action.users,
      });
    case UPDATE_ROUTE:
      return Object.assign({}, state, {
        mapRoute: action.mapRoute,
      });
    case 'newUser':
      console.log(action);
      return Object.assign({}, state, {
        users: [...state.users, action.user],
      });
    case 'removeUser':
      console.log(action);
      let removeIndex = state.users.findIndex(user => user.username === action.user.username);

      return Object.assign({}, state, {
        users: [...state.users.slice(0, removeIndex), ...state.users.slice(removeIndex + 1)],
      });
    case 'toggleAnimate':
      console.log(action);
      let animateIndex = state.users.findIndex(user => user.username === action.user.username);

      return Object.assign({}, state, {
        users: [...state.users.slice(0, animateIndex), ...state.users.slice(animateIndex + 1), action.user],
      });
    default:
      return state;
  }
};

export default AppReducer;
