import React, { PropTypes, Component } from 'react';

// Import Style
import styles from './AuthUser.css';
// Import Images
import logo from '../../Logo.png';

export class AuthUser extends Component {
  handleClick(event) {
    const username = this.refs.username;
    const password = this.refs.password;
    const creds = { username: username.value.trim(), password: password.value.trim() };
    this.props.onLoginClick(creds);
  }

  render() {
    const { errorMessage } = this.props;

    return (
        <div className={styles.container}>
            <img src={require('../../Logo.png')} className={styles.loginLogo} />
            <p>Logga in för att fortsätta</p>
            <input type="text" ref="username" className={styles.loginForm} placeholder="Username" />
            <input type="password" ref="password" className={styles.passwordForm} placeholder="Password" />
            <input type="submit" value="Logga in" onClick={(event) => this.handleClick(event)} className={styles.btn} />

            {errorMessage &&
                <p>{errorMessage}</p>
            }
        </div>
    );
  }
}

export default AuthUser;
