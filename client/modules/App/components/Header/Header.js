import React, { PropTypes } from 'react';

// Import Style
import styles from './Header.css';

export const Header = props => (
  <div style={{
    display: props.name ? 'block' : 'inline-block',
    width: props.name ? '100%' : 'auto',
  }} className={styles.header}
  >
    <div className={styles.content}>
      <img src={require('../../Logo.png')} className={styles.headerLogo} />
    </div>
    {props.name &&
      <div className={styles.user}>
        <span>inloggad som</span>
        {props.name}
      </div>
    }
  </div>
);


Header.contextTypes = {
  router: React.PropTypes.object,
};

export default Header;
