export const REQUEST_ROUTE = 'REQUEST_ROUTE';
export const RECIEVE_ROUTE = 'RECIEVE_ROUTE';
export const FETCH_ROUTE_REQUEST = 'FETCH_ROUTE_REQUEST';
export const FETCH_ROUTE_FAILURE = 'FETCH_ROUTE_FAILURE';
export const FETCH_ROUTE_SUCCESS = 'FETCH_ROUTE_SUCCESS';
export const STORE_MAP = 'STORE_MAP';
export const ADD_WAYPT = 'ADD_WAYPT';
export const DELETE_WAYPT = 'DELETE_WAYPT';
export const TOGGLE_ANIMATE = 'TOGGLE_ANIMATE';

const requestRoute = request => {
  return {
    type: REQUEST_ROUTE,
    ...request,
  };
};

const receiveRoute = (request, calculations) => {
  return {
    type: RECIEVE_ROUTE,
    ...request,
    mapRoute: calculations,
    receivedAt: Date.now(),
  };
};

const fetchError = status => {
  return {
    type: FETCH_ROUTE_FAILURE,
    errorMessage: `Something went terribly wrong trying to fetch new route due to: ${status}`,
  };
};

export const storeMap = map => {
  return {
    type: STORE_MAP,
    mapObj: map,
  };
};

export const addWaypt = waypt => {
  return {
    type: ADD_WAYPT,
    waypt,
  };
};

export const deleteWaypt = waypts => {
  return {
    type: DELETE_WAYPT,
    waypts,
  };
};

export const fetchRoute = (request, mapObj) => {
  const { directionsService, directionsDisplay } = mapObj.services;
  directionsDisplay.setMap(mapObj.map);
  directionsDisplay.setOptions({ suppressMarkers: true, preserveViewport: true });
  return dispatch => {
    dispatch(receiveRoute(request));

    return directionsService.route({
      ...request,
    }, (response, status) => {
      if (status === 'OK') {
        directionsDisplay.setDirections(response);

        const calculations = response.routes[0].legs.map(elements => {
          return {
            address: elements.end_address,
            distance: elements.distance.value,
            duration: elements.duration.value,
          };
        });

        dispatch(receiveRoute(request, calculations));
      } else {
        dispatch(fetchError(status));
      }
    });
  };
};

