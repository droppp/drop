import {
  REQUEST_ROUTE,
  RECIEVE_ROUTE,
  FETCH_ROUTE_REQUEST,
  FETCH_ROUTE_FAILURE,
  FETCH_ROUTE_SUCCESS,
  STORE_MAP,
  ADD_WAYPT,
  DELETE_WAYPT,
} from './GMapActions';

const initialState = {
  waypts: [],
  mapLoaded: false,
  mapRoute: [],
  mapObj: {},
};

const GMapReducer = (state = initialState, action) => {
  switch (action.type) {
    case REQUEST_ROUTE:
      return Object.assign({}, state, {
        isFetching: true,
        didInvalidate: false,
      });
    case RECIEVE_ROUTE:
      return Object.assign({}, state, {
        isFetching: false,
        didInvalidate: false,
        mapRoute: action.mapRoute,
        lastUpdated: action.receivedAt,
      });
    case FETCH_ROUTE_FAILURE:
      return Object.assign({}, state, {
        errorMessage: action.errorMessage,
        isFetching: false,
      });
    case STORE_MAP:
      return Object.assign({}, state, {
        mapObj: action.mapObj,
        mapLoaded: !state.mapLoaded,
      });
    case ADD_WAYPT:
      return Object.assign({}, state, {
        waypts: [...state.waypts, action.waypt],
      });
    case DELETE_WAYPT:
      return Object.assign({}, state, {
        waypts: action.waypts,
      });

    default:
      return state;
  }
};

export default GMapReducer;
