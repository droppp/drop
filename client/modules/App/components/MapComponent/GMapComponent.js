import React, { Component } from 'react';
import { connect } from 'react-redux';
import { mapStyle } from './GMapStyle';
import { loadJS } from '../../../../util/helpers';
import { fetchRoute, storeMap, addWaypt, deleteWaypt } from './GMapActions';
import { toggleAnimate, fetchActiveUsers } from '../../AppActions';

import styles from './GMapComponent.css';

export class GMapComponent extends Component {

  constructor(props) {
    super(props);
    this.markers = [];
    this.userMarkers = [];
    this.timeout = null;
  }

  renderMarkersFromArray() {
    if (!this.props.mapLoaded || !this.props.mapObj) {
      return;
    }

    const { user, users, mapObj } = this.props;
    const bounds = new google.maps.LatLngBounds();

    const markers = users.map(element => {
      if (element.fname !== user.fname) {
        const marker = new google.maps.Marker({
          position: element.coordinates,
          icon: {
            url: require('../../icons/dropper.svg'),
            fillColor: '#FF0000',
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(25, 50),
          },
          map: mapObj.map,
        });
        const newWaypt = {
          location: element.coordinates,
          stopover: true,
        };
        this.markers.push(marker);
        this.userMarkers.push({ element, marker });
        marker.addListener('click', () => {
          this.checkArray(newWaypt);
        });

        return marker;
      } else {
        return null;
      }
    });

    this.markers.forEach((el, index, arr) => {
      bounds.extend(el.getPosition());
    });
    this.animateMarker(google.maps.Animation.BOUNCE);
    if (this.timeout !== null) {
      clearTimeout(this.timeout);
    }
    this.timeout = setTimeout(() => this.animateMarker(null), 10000);

    mapObj.map.fitBounds(bounds);
    return markers;
  }

  animateMarker(animate) {
    const { dispatch } = this.props;
    const markers = this.userMarkers.filter(marker => marker.element.animate === true);
    if (markers.length > 0) {
      markers.forEach((el, index, arr) => {
        el.marker.setAnimation(animate);
      });
    }
  }

  calculateAndDisplayRoute = () => {
    if (!this.props.mapObj) {
      return;
    }
    const { user, destAddress, waypts, mapObj } = this.props;

    const request = {
      origin: user.coordinates,
      destination: { lat: 59.346082, lng: 18.058314 },
      waypoints: waypts,
      optimizeWaypoints: true,
      travelMode: 'DRIVING',
    };

    console.log(mapObj);
    this.props.dispatch(fetchRoute(request, mapObj));
  }

  checkArray(newWaypt, remove = false) {
    const { waypts } = this.props;
    const isInArr = waypts.filter(value => {
      return value.location.lat === newWaypt.location.lat &&
                   value.location.lng === newWaypt.location.lng;
    });
    if (isInArr.length > 0) {
      this.removeCoordinates(newWaypt);
    } else if (!remove) {
      this.attachCoordinates(newWaypt);
    }
  }

  attachCoordinates(newWaypt) {
    const { dispatch } = this.props;
    dispatch(addWaypt(newWaypt));
  }

  removeCoordinates(removeWaypt) {
    const { dispatch, waypts } = this.props;
    const newWaypts = waypts.filter(value => {
      return value.location.lat !== removeWaypt.location.lat &&
                value.location.lng !== removeWaypt.location.lng;
    });
    dispatch(deleteWaypt(newWaypts));
  }

  componentDidMount() {
    window.initMap = this.initMap;
    loadJS('https://maps.googleapis.com/maps/api/js?key=AIzaSyBloW3X026osS65tob_zL-h0boNIDcvRD4&callback=initMap');

    this.props.dispatch(fetchActiveUsers());
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.mapLoaded !== nextProps.mapLoaded) {
      return true;
    }
    if (this.props.waypts !== nextProps.waypts) {
      return true;
    }
    else if (this.props.users !== nextProps.users) {
      return true;
    }
    return false;
  }

  componentWillUpdate() {
    this.userMarkers.forEach((el, index, arr) => {
      el.marker.setMap(null);
    });
    this.userMarkers = [];
  }

  componentDidUpdate(prevProps, prevState) {
    const { dispatch } = this.props;

    if (this.props.mapLoaded && this.props.mapObj !== null) {
      console.log('render markers');
      this.renderMarkersFromArray();
      this.calculateAndDisplayRoute();
    }

    if (
      prevProps.users.length > this.props.users.length
      && prevProps.mapRoute.length == this.props.mapRoute.length
      && this.props.mapLoaded
      && this.props.mapObj !== null
    ) {
      const myObject = prevProps.users.find(el => this.props.users.indexOf(el) < 0);
      console.log(`users: ${prevProps.users.length} och ${this.props.users.length}`);
      console.log(`mapRoute: ${prevProps.mapRoute.length} och ${this.props.mapRoute.length}`);
      console.log(myObject);
      if (myObject) {
        const newWaypt = { location: myObject.coordinates };
        this.checkArray(newWaypt, true);
      }
    }
  }

  initMap = () => {
    console.log(google);
    const { user, dispatch } = this.props;
    const services = {
      directionsService: new google.maps.DirectionsService,
      directionsDisplay: new google.maps.DirectionsRenderer({ polylineOptions: { strokeColor: '#669999' } }),
    };
    const defaultOptions = {
      center: user.coordinates,
      scrollwheel: false,
      zoom: 11,
      disableDefaultUI: true,
    };
    const map = new google.maps.Map(this.refs.map, defaultOptions);
    const userMarker = new google.maps.Marker({
      position: user.coordinates,
      animation: google.maps.Animation.DROP,
      icon: {
        url: require('../../icons/home.svg'),
        fillColor: '#FF0000',
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(25, 50),
        strokeWeight: 1,
        scale: 1,
      },
      map,

    });
    const destMarker = new google.maps.Marker({
      position: { lat: 59.346082, lng: 18.058314 },
      animation: google.maps.Animation.DROP,
      icon: {
        url: require('../../icons/destination.svg'),
        fillColor: '#FF0000',
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(25, 50),
        strokeWeight: 1,
        scale: 1,
      },
      map,
    });
    this.markers.push(userMarker, destMarker);
    map.setOptions({ styles: mapStyle });

    const mapObj = {
      map,
      services,
    };

    dispatch(storeMap(mapObj));
  }


  render() {
    return (
        <div className={styles.map} ref="map"></div>
    );
  }


}

const mapStateToProps = state => {
  return { ...state.gmap };
};

export default connect(mapStateToProps)(GMapComponent);
