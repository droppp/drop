import React, { Component } from 'react';
import { Link } from 'react-router';
import HamburgerMenu from 'react-hamburger-menu';
import { Motion, spring } from 'react-motion';

import styles from './MenuButton.css';

export class MenuButton extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isPressed: false,
    };
  }

  handleTouchStart = (e) => {
    this.setState({
      isPressed: true,
    });
  }

  handleTouchEnd = () => {
    this.setState({
      isPressed: false,
    });
  }

  render() {
    const { menuOpen, toggleMenu } = this.props;

    return (
            <Motion
              defaultValue={{ x: 1, y: -50 }}
              style={{ x: this.state.isPressed ? spring(1.1, { stiffness: 260, damping: 7 }) : spring(1, { stiffness: 260, damping: 7 }), y: -50 }}
            >
                {({ x, y }) =>
                    <div
                      onTouchStart={this.handleTouchStart.bind()}
                      onTouchEnd={this.handleTouchEnd.bind()}
                      className={styles.container}
                      style={{
                        WebkitTransform: `scale(${x}) translateX(${y}%)`,
                        transform: `scale(${x}) translateX(${y}%)`,
                      }}
                    >
                        <div className={styles.svg}>
                            <HamburgerMenu
                              isOpen={menuOpen}
                              menuClicked={toggleMenu.bind(this)}
                              width={30}
                              height={20}
                              strokeWidth={3}
                              rotate={0}
                              color="black"
                              borderRadius={0}
                              animationDuration={0.2}
                            />
                        </div>
                    </div>
                }
            </Motion>
        );
  }
}

export default MenuButton;
