import { API_URL, callApi } from '../../util/apiCaller';

// Export Constants
export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';
export const FETCH_USERS = 'FETCH_USERS';
export const MENU_OPEN = 'MENU_OPEN';
export const MENU_CLOSE = 'MENU_CLOSE';
export const UPDATE_ROUTE = 'UPDATE_ROUTE';
export const TOGGLE_ANIMATE = 'TOGGLE_ANIMATE';


export function toggleMenu(open) {
  return open ? { type: MENU_CLOSE } : { type: MENU_OPEN };
}

export function updateRouteSummary(arr) {
  return {
    type: UPDATE_ROUTE,
    mapRoute: arr,
  };
}

function requestLogin(creds) {
  return {
    type: LOGIN_REQUEST,
    isFetching: true,
    isAuthenticated: false,
    creds,
  };
}

function receiveLogin(user) {
  return {
    type: LOGIN_SUCCESS,
    isFetching: false,
    isAuthenticated: true,
    id_token: user.token,
    user,
  };
}

function loginError(message) {
  return {
    type: LOGIN_FAILURE,
    isFetching: false,
    isAuthenticated: false,
    message,
  };
}

export function loginUser(creds) {
  let config = {
    method: 'POST',
    headers: { 'content-type': 'application/json' },
    body: JSON.stringify(creds),
  };

  return dispatch => {
    // We dispatch requestLogin to kickoff the call to the API
    dispatch(requestLogin(creds));

    return fetch(`${API_URL}/authenticate`, config)
      .then(response =>
        response.json().then(payload => ({ payload, response }))
          ).then(({ payload, response }) => {
            if (!response.ok) {
              // If there was a problem, we want to
              // dispatch the error condition
              dispatch(loginError(payload.message));
              return Promise.reject(payload);
            } else {
              if (!payload.success) {
                dispatch(loginError(payload.message));
                return Promise.reject(payload);
              } else {
                // If login was successful, set the token in local storage
                // localStorage.setItem('id_token', payload.token)
                // Dispatch the success action
                dispatch(receiveLogin(payload));
              }
            }
          }).catch(err => console.log('Error: ', err));
  };
}

export function fetchActiveUsers() {
  return (dispatch) => {
    return callApi('users').then(res => {
      dispatch(addUsers(res.users));
    });
  };
}

export function turnAnimateStateOff(state, username) {
  return dispatch => {
    return callApi('user/animate', 'post', {
      username,
      animate: state,
    }).then(res => {
      dispatch(toggleAnimate());
    });
  };
}

export function toggleAnimate() {
  return {
    type: TOGGLE_ANIMATE,
  };
}

function addUsers(users) {
  return {
    type: FETCH_USERS,
    users,
  };
}

