import React, { Component, PropTypes } from 'react';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

// Import Style
import styles from './App.css';
import animations from './animations.css';

// Import Components
import Helmet from 'react-helmet';
import DevTools from './components/DevTools';
import Header from './components/Header/Header';
import AuthUser from './components/AuthUser/AuthUser';
// import Footer from './components/Footer/Footer';
import GMapComponent from './components/MapComponent/GMapComponent';
import MenuButton from './components/MenuButton/MenuButton';

import { loginUser, toggleMenu, fetchActiveUsers } from './AppActions';

export class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isMounted: false,
    };
  }

  componentDidMount() {
    const { token } = this.props.userInfo;
    this.setState({isMounted: true}); // eslint-disable-line
  }

  componentDidUpdate() {

  }

  toggleMenuOpen = () => {
    this.props.menuOpen ? browserHistory.push('/') : browserHistory.push('/menu');
    this.props.dispatch(toggleMenu(this.props.menuOpen));
  }

  render() {
    const { dispatch, isAuthenticated, errorMessage, isFetching, userInfo, menuOpen, users, updateRouteSummary } = this.props,
      { user } = userInfo,
      path = this.props.location.pathname,
      segment = path.split('/')[1] || 'root';

    return (
      <div>
        {this.state.isMounted && !window.devToolsExtension && process.env.NODE_ENV === 'development'}

        <div className={styles.container}>
          <Helmet
            title="DROP"
            titleTemplate="%s - The Ultimate Carpooling App"
            meta={[
              { charset: 'utf-8' },
              {
                'http-equiv': 'X-UA-Compatible',
                content: 'IE=edge',
              },
              {
                name: 'viewport',
                content: 'width=device-width, initial-scale=1',
              },
            ]}
          />
          {isAuthenticated &&
          <Header />
          }
          <ReactCSSTransitionGroup
            transitionName={animations}
            transitionAppearTimeout={500}
            transitionAppear
            transitionEnterTimeout={500}
            transitionLeaveTimeout={300}
          >

              {React.cloneElement(this.props.children, { key: segment })}

          </ReactCSSTransitionGroup>

          {!isAuthenticated &&
            <AuthUser
              errorMessage={errorMessage}
              onLoginClick={creds => dispatch(loginUser(creds))}
            />
          || isAuthenticated &&
            <MenuButton
              toggleMenu={this.toggleMenuOpen}
              menuOpen={menuOpen}
            />
          }
        </div>
        {isAuthenticated &&
        <GMapComponent
          user={user}
          users={users}
          destAddress={'Tulegatan 41'}
        />}

      </div>
    );
  }
}

App.propTypes = {
  // children: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
};

// Retrieve data from store as props
const mapStateToProps = state => {
  return { ...state.app };
};

export default connect(mapStateToProps)(App);
