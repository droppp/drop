import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { FormattedMessage } from 'react-intl';

// Import Style
import styles from './UserListItem.css';

function UserListItem(props) {
  return (
    <div className={styles['single-post']}>
      <h3 className={styles['post-title']}>
        <Link>
          {props.user.fname}
        </Link>
      </h3>
      <p className={styles['author-name']}><FormattedMessage id="by" /> {props.user.fname} {props.user.lname}</p>
      <p className={styles['post-desc']}>{props.user.address}</p>
      <hr className={styles.divider} />
    </div>
  );
}

UserListItem.propTypes = {
  user: PropTypes.shape({
    fname: PropTypes.string.isRequired,
    lname: PropTypes.string.isRequired,
    address: PropTypes.string.isRequired,
    slug: PropTypes.string.isRequired,
    cuid: PropTypes.string.isRequired,
  }).isRequired,
};

export default UserListItem;
