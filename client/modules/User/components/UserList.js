import React, { PropTypes } from 'react';

// Import Components
import UserListItem from './UserListItem/UserListItem';

function UserList(props) {
  return (
     <div className="listView">
      {
        props.users.map(user => (
            <UserListItem
              user={user}
              key={user.cuid}
            />
        ))
      }
    </div>
  );
}

UserList.propTypes = {
  users: PropTypes.arrayOf(PropTypes.shape({
    fname: PropTypes.string.isRequired,
    lname: PropTypes.string.isRequired,
    address: PropTypes.string.isRequired,
    slug: PropTypes.string.isRequired,
    cuid: PropTypes.string.isRequired,
  })).isRequired,
};

export default UserList;
