import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';

// Import Components
import UserList from '../../components/UserList';

// Import Actions
import { fetchUsers } from '../../UserActions';

// Import Selectors
import { getUsers } from '../../UserReducer';

// Import styles
import styles from './UserListPage.css';

class UserListPage extends Component {

  componentDidMount() {
    this.props.dispatch(fetchUsers());
  }

  render() {
    return (
      <div className={styles.userlistContainer}>
        <h1>UserLIst!!!!11!1!11</h1>
        <UserList users={this.props.users} />
      </div>
    );
  }
}

// Actions required to provide data for this component to render in sever side.
UserListPage.need = [() => { return fetchUsers(); }];

// Retrieve data from store as props
function mapStateToProps(state) {
  return {
    users: getUsers(state),
  };
}

UserListPage.propTypes = {
  users: PropTypes.arrayOf(PropTypes.shape({
    fname: PropTypes.string.isRequired,
    lname: PropTypes.string.isRequired,
    cuid: PropTypes.string.isRequired,
  })).isRequired,
  dispatch: PropTypes.func.isRequired,
};

UserListPage.contextTypes = {
  router: React.PropTypes.object,
};

export default connect(mapStateToProps)(UserListPage);
