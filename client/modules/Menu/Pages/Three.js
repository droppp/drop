import React, { Component } from 'react';
import styles from './Three.css';

class RequestRide extends Component {

  render() {
    return (
        <div className={styles.container}>
          <div className={styles.content}>
            <h3 className={styles.heading}>Vill du få skjuts?</h3>
            <p>Ändra status till aktiv för att andra i gruppen ska kunna se att du är tillgänglig för upphämtning</p>

            <p>Status: {this.props.active ? 'Aktiv' : 'Inaktiv'}</p>
            <button style={{
              background: this.props.active ? 'rgba(61, 92, 90, 1)' : 'rgba(64, 64, 64, 1)',
            }} onClick={this.props.toggleActive}
            >
              {this.props.active ? 'Avaktivera' : 'Aktivera'}
            </button>
          </div>
        </div>
    );
  }
}

export default RequestRide;
