import React, { Component } from 'react';
import styles from './Two.css';

export class Two extends Component {
  render() {
    return (
            <div className={styles.container}>
                <h3 className={styles.heading}>Two</h3>
            </div>
        );
  }
}

export default Two;
