import React, { Component } from 'react';
import RouteSummary from '../RouteSummary';
import styles from './One.css';

export class One extends Component {
  render() {
    return (
            <div className={styles.container}>
                <RouteSummary
                  mapRoute={this.props.mapRoute}
                />
            </div>
        );
  }
}

export default One;
