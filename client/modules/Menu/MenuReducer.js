import { TOGGLE_ACTIVE, GET_ACTIVE_STATE } from './MenuActions';

// The auth reducer. The starting state sets authentication
// based on a token being in local storage. In a real app,
// we would also want a util to check if the token is expired.
const MenuReducer = (state = {
  active: false,
  animate: false,
}, action) => {
  switch (action.type) {
    case TOGGLE_ACTIVE:
      return Object.assign({}, state, {
        active: !state.active,
      });
    case GET_ACTIVE_STATE:
      return Object.assign({}, state, {
        active: action.active,
      });

    default:
      return state;
  }
};

export default MenuReducer;
