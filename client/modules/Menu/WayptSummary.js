import React from 'react';
import { formatDuration, formatDistance } from '../../util/helpers';

import styles from './RouteSummary.css';

const WayptSummary = ({ index, address, distance, duration }) => (
  <li className={styles.listItem}>
    <div className={styles.index}>
      <span>{index = index + 1}</span>
    </div>
    <div className={styles.address}>
      {address}
    </div>
    <div className={styles.distance}>
      {formatDistance(distance)}
    </div>
    <div className={styles.duration}>
      {formatDuration(duration)}
    </div>

  </li>
);

export default WayptSummary
;
