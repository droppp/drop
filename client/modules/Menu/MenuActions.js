import { API_URL, callApi } from '../../util/apiCaller';

export const TOGGLE_ACTIVE = 'TOGGLE_ACTIVE';
export const GET_ACTIVE_STATE = 'GET_ACTIVE_STATE';

export const toggleActive = () => {
  return {
    type: TOGGLE_ACTIVE,
  };
};

export const getActiveState = state => {
  return {
    type: GET_ACTIVE_STATE,
    active: state,
  };
};

const emitActiveUser = (state, username) => {
  return {
    type: 'server/add',
    username,
    active: state,
  };
};


export const setActiveState = (state, username) => {
  return dispatch => {
    return callApi('user/active', 'post', {
      username,
      active: state,
    }).then(res => dispatch(emitActiveUser(state, username))).then(res => {
      console.log(res);
      dispatch(toggleActive());
    });
  };
};
