import React, { Component } from 'react';
import { formatDuration, formatDistance } from '../../util/helpers';
import WayptSummary from './WayptSummary';

// Import Style
import styles from './RouteSummary.css';

class RouteSummary extends Component {
  constructor(props) {
    super(props);
    this.totalDistance = 0;
    this.totalDuration = 0;

    props.mapRoute.forEach((element, index, array) => {
      this.totalDistance = this.totalDistance + element.distance;
      this.totalDuration = this.totalDuration + element.duration;
    });

    this.mapRoute = props.mapRoute.slice(0);
    this.destination = this.mapRoute.pop();
  }

  render() {
    return (
      <div className={styles.container}>
        <h3>Planerad Rutt</h3>
        <div>
          <div className={styles.destination}>
            <span>Slutdestination</span>
            {this.destination && this.destination.address}
            <div className={styles.total}>
              <div>
                <span>Total distans</span>
                {formatDistance(this.totalDistance)}
              </div>
              <div>
                <span>Total tid</span>
                {formatDuration(this.totalDuration)}
              </div>
            </div>
          </div>
          <div className={styles.arrayHeading}>{this.mapRoute.length !== 0 ? 'Stopp på vägen' : 'Inga stopp på vägen'}</div>
          <ul className={styles.list}>
          {this.mapRoute.map((route, index) =>
              <WayptSummary
                key={route.address}
                index={index}
                {...route}
              />
          )}
          </ul>
        </div>
      </div>
    );
  }
}

export default RouteSummary;
