import React, { Component } from 'react';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import Swipeable from 'react-swipeable';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import io from 'socket.io-client';
import Header from '../App/components/Header/Header';
import { toggleActive, getActiveState, setActiveState } from './MenuActions';

import styles from './Menu.css';
import animations from './animations.css';
import reverseAnimations from './reverseAnimations.css';

export class Menu extends Component {

  constructor(props) {
    super(props);
    this.state = {
      page: 0,
    };
  }

  componentWillMount() {
    const { isAuthenticated, dispatch, userInfo } = this.props;

    dispatch(getActiveState(userInfo.user.active));

    if (!isAuthenticated) {
      browserHistory.push('/');
    }
  }

  getAnimationDirection = () => {
    switch (this.state.direction) {
      case 'right':
        return animations;
      case 'left':
        return reverseAnimations;
      default:
        return animations;
    }
  }

  switchPage() {
    switch (this.state.page) {
      case 1:
        browserHistory.push('/menu/two');
        break;
      case 2:
        browserHistory.push('/menu/three');
        break;
      default:
        browserHistory.push('/menu');
        break;
    }
  }

  swipedLeft = () => {
    if (this.state.page === 2) {
      this.setState({
        page: 0,
      });
    } else {
      this.setState({
        page: this.state.page + 1,
      });
    }

    this.setState({
      direction: 'right',
    }, this.switchPage);
  }

  swipedRight = () => {
    if (this.state.page === 0) {
      this.setState({
        page: 2,
      });
    } else {
      this.setState({
        page: this.state.page - 1,
      });
    }

    this.setState({
      direction: 'left',
    }, this.switchPage);
  }

  swipedDown = () => {

  }

  cloneChildren() {
    var path = this.props.location.pathname;
    if (this.props.children) {
      return React.cloneElement(this.props.children, {
        key: path,
        mapRoute: this.props.mapRoute,
        active: this.props.active,
        toggleActive: () => this.props.dispatch(setActiveState(this.props.active, this.props.userInfo.user.username)),
      });
    }
  }

  render() {
    const { user } = this.props.userInfo;
    const path = this.props.location.pathname;
    const segment = path.split('/')[2] || 'root';
    return (
      <div className={styles.container}>
        <Header
          name={`${user.fname} ${user.lname}`}
        />

        <Swipeable
          className={styles.swipeable}
          onSwipedLeft={this.swipedLeft}
          onSwipedRight={this.swipedRight}
          onSwipedDown={this.swipedDown}
        >
          <ReactCSSTransitionGroup
            transitionName={this.getAnimationDirection()}
            transitionAppearTimeout={500}
            transitionAppear
            transitionEnterTimeout={500}
            transitionLeaveTimeout={300}
          >

            {this.cloneChildren()}

        </ReactCSSTransitionGroup>
        </Swipeable>
      </div>
    );
  }
}

// Retrieve data from store as props
const mapStateToProps = state => {
  const { mapRoute } = state.gmap;
  return {
    ...state.app,
    ...state.menu,
    mapRoute,
  };
};

export default connect(mapStateToProps)(Menu);
