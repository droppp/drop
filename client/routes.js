/* eslint-disable global-require */
import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './modules/App/App';

// require.ensure polyfill for node
if (typeof require.ensure !== 'function') {
  require.ensure = function requireModule(deps, callback) {
    callback(require);
  };
}

/* Workaround for async react routes to work with react-hot-reloader till
  https://github.com/reactjs/react-router/issues/2182 and
  https://github.com/gaearon/react-hot-loader/issues/288 is fixed.
 */
if (process.env.NODE_ENV !== 'production') {
  // Require async routes only in development for react-hot-reloader to work.
  require('./modules/Post/pages/PostListPage/PostListPage');
  require('./modules/Post/pages/PostDetailPage/PostDetailPage');
  require('./modules/User/pages/UserListPage/UserListPage');
}

const MockIndex = () => (
  <div></div>
);

// react-router setup with code-splitting
// More info: http://blog.mxstbr.com/2016/01/react-apps-with-pages/
export default (
  <Route path="/" component={App}>
    <IndexRoute component={MockIndex} />
    <Route
      path="posts"
      getComponent={(nextState, cb) => {
        require.ensure([], require => {
          cb(null, require('./modules/Post/pages/PostDetailPage/PostDetailPage').default);
        });
      }}
    />
    <Route
      path="users"
      getComponent={(nextState, cb) => {
        require.ensure([], require => {
          cb(null, require('./modules/User/pages/UserListPage/UserListPage').default);
        });
      }}
    />
    <Route
      path="menu"
      getComponent={(nextState, cb) => {
        require.ensure([], require => {
          cb(null, require('./modules/Menu/Menu').default);
        });
      }}
    >
      <IndexRoute
        getComponent={(nextState, cb) => {
          require.ensure([], require => {
            cb(null, require('./modules/Menu/Pages/One').default);
          });
        }}
      />
      <Route
        path="two"
        getComponent={(nextState, cb) => {
          require.ensure([], require => {
            cb(null, require('./modules/Menu/Pages/Two').default);
          });
        }}
      />
      <Route
        path="three"
        getComponent={(nextState, cb) => {
          require.ensure([], require => {
            cb(null, require('./modules/Menu/Pages/Three').default);
          });
        }}
      />
    </Route>
  </Route>
);
