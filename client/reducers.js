/**
 * Root Reducer
 */
import { combineReducers } from 'redux';

// Import Reducers
import app from './modules/App/AppReducer';
import gmap from './modules/App/components/MapComponent/GMapReducer';
import menu from './modules/Menu/MenuReducer';
import posts from './modules/Post/PostReducer';
import users from './modules/User/UserReducer';
import intl from './modules/Intl/IntlReducer';

// Combine all reducers into one root reducer
export default combineReducers({
  app,
  gmap,
  menu,
  posts,
  users,
  intl,
});
