export const loadJS = src => {
  const ref = window.document.getElementsByTagName('script')[0];
  const script = window.document.createElement('script');
  script.src = src;
  script.async = true;
  ref.parentNode.insertBefore(script, ref);
};

export const formatDuration = time => {
  const hrs = Math.floor(time / 3600);
  const mins = Math.floor((time % 3600) / 60);

  return hrs ? `${hrs}h ${mins} minuter` : `${mins} minuter`;
};

export const formatDistance = distance => {
  const km = Math.floor(distance / 1000);

  return `${km} km`;
};
