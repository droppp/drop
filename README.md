

![alt text](https://bytebucket.org/droppp/drop/raw/ea058a4c386289a8386359db30bf719261ddb336/client/modules/App/Logo.png "Wireframes")
# DROP - The Ultimate Carpooling App 



> ”Jorden är 4.6 miljarder år gammal. Låt oss skala ner detta till 46 år. Männniskan har då funnits här i 4 timmar. Industrialismen startade för en minut sedan. Under den minuten har vi förstört mer än 50% av världens skogar. Detta är inte hållbart”

###### Människans inverkan på miljö och klimat blir mer och mer påtagligt varje dag. Vi måste agera nu. Förbränningsmotorns dagar borde sedan länge varit räknade. Tyvärr delar inte alla människor den åsikten(läs Trump). Vi måste därför göra allt vi kan för att minska människans användning av bilen i vardagen. Vi tittar på ett exempel baserat på en familj med tre barn.


###### En vardag med bil:
+ Skjutsa barn till skolan
+ åka till jobbet
+ åka från jobbet
+ hämta barn från skolan
+ skjutsa barn 2 till cirkusskola
+ Hämta barn 2 från cirkusskola
+ lämna barn 1 på fotbollsträning
+ hämta barn 3 hos kompis
+ hämta barn 3 glömda vantar och mössa hos kompis
+ hämta barn 1 från fotbollsträning

###### Vi vill ta fram en app/tjänst som förenklar livet för denna förälder och samtidigt gör miljön en stor tjänst. En tjänst som gör samåkning till en busenkel självklarhet.


[![Video](http://img.youtube.com/vi/_uGXV74bVe4/0.jpg)](https://www.youtube.com/watch?v=_uGXV74bVe4&feature=youtu.be)

> video demo (youtube)

### 1. Wireframe & flöde 

![alt text](https://bytebucket.org/droppp/drop/raw/b3b77b64aabaed6843f8cf48d10e6f862d58916e/wireframes.png "Wireframes")

Landningssidan består av en inloggningsskärm. När man loggat in så möts man av en karta. På kartan syns två Drop-ikoner, en hemikon på och en destinationsikon. Däremellan presenteras en uträknad rutt. Man ser även andra Drop-ikoner som symboliserar folk som vill bli upphämtade. Man klickar på de ikoner som man kan tänka sig plocka upp på vägen och rutten räknas automatiskt om.

Ett klick på meny-ikonen så dyker en detaljerad plan upp. Där kan man läsa hur långt det är till slutdestinationen, hur lång tid det tar att åka dit samt vilken tid man förväntas anlända. På de andra menysidorna kan signalera om man vill bli upphämtad och även redigera sin profilinformatioin. Om en person väljer att den vill bli upphämtad, så ser alla inloggade att det droppar ner en ikon på den personens plats.

Tanken med själva flödet är att det ska vara smidigt att navigera i. En person ska enkelt komma åt de viktigaste funktionerna genom att bara använda en hand. Vi har därför placerat all funktion i den nedre halvan av telefonen, för att man ska kunna komma åt allt med tummen utan att behöva justera greppet.

Även menyn har vi valt att placera längst ner på skärmen. Till själva utformning av menyn har vi tagit inspiration från gamla telefoners rullhjul. För att komma till nästa sida snurrar man på hjulet, alternativt ”greppar” bakgrunden och föser den åt åt sidan.



### 2. Beroenden, krav, begränsningar och målplattformar
Vår app är i dagsläget utvecklad endast för mobil användning, då vi vill att användaren av appen ska vara så rörlig som möjligt. Planen är att implementera bättre stöd för desktop i framtiden men huvudfokus kommer alltid ligga på den mobila användaren. Vi använder oss av Google Maps Javascript API och Google Maps Directions API, båda i version 3. Vi använder oss även av JSON Web Token för att autensiera användare.


### 3. Kodens struktur
Kodstrukturen är baserad på en boilerplate som heter MERN Starter från mern.io. 
På framsidan finns en client som renderar en App. Appen renderar ut all html. I startläget renderar App ut komponenten AuthUser, ett inloggningsformulär. När användaren loggar in görs en jämförelse av inloggningsuppgifterna i Main.controller.js på servern. Om dem stämmer överens genereras en tillfällig Webtoken som returneras till client. När en användare är inloggad laddar App in modulen MapComponent. MapComponent kör ett script som heter LoadJS som kör igång en asynchron laddning av Google Maps. Vi hämtar alla användare som matchar villkoren via API och lagrar dem i vårt State. Sedan hämtar skickar vi in koordinaterna till Google Maps tillsammans med start och mål koordinaterna. 


### 4. Extern kod 
Vi valde denna boilerplate för att snabbt komma igång med en kraftfull fullstack applikation. Vi ville också ha en bra grundstruktur att stå på, då ingen av oss suttit i något större projekt i React. Nackdelen med att använda sig av denna stora boilerplate var att det var extremt mycket kod och dokumentation att sätta sig in i innan man började förstå hur allt lirade. Och några delar av paketet var överdimensionerade för just denna app.


### 5. Processen
Vi har jobbat agilt med DROP, ett SCRUM-like arbetsätt. Vi har försökt dela upp projektet i många små-tasks. Vi har brutit ner alla problem vi stött på under projektets gång i små-tasks. Vi bestämde att en sprint skulle vara en vecka lång, förutom över jul och nyår då sprinten sträckte sig två veckor på grund av röda dagar. Eftersom vi har suttit mycket tillsammans och bollat idéer fram och tillbaka kände vi snabbt att standups och retrospectives var överflödiga, så dem strök vi. Vi satte upp ett git-repository på Bitbucket.org som vi jobbade mot under projektet. 


### 6. Kunskapsutveckling
Vi känner att vi har fått en mycket djupare insikt i Javascript och de ramverk som användes i MERN stacken. Detta var något som vi båda hade som avsikt från start då vi tyckte att skolan hade belyst dessa områden relativt lite. Vi fick speciellt mycket insikt i React och hur kraftfullt det blir ihop med Redux. Vi fick även en bra inblick i att sätta upp och jobba med APIer mot NoSQL databaser i MongoDB via Express.


### 7. Överstigande leverans.

Vi är särskilt stolta över vår liveuppdatering på användare som vill bli upphämtade. Vi använder oss utav socket.io som en brygga mellan server och Redux. Socket lyssnar på serversidan efter att redux dispatchar en specifik action. Sedan broadcastar Socket en användare beroende på vilket state som skickats med i Redux action-objektet.

